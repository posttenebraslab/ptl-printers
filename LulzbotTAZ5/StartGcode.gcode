;START GCODE
;This G-Code has been generated specifically for the LulzBot TAZ 5 with standard extruder
M75			 ; start GLCD timer

M140 S{material_bed_temperature_layer_0}    ; start bed heating up
M105
M104 S{material_print_temperature_layer_0}  ; start extruder heat
M105
G90                      ; absolute positioning
M107                     ; disable fans
M82                      ; set extruder to absolute mode
G28 X0 Y0                ; home X and Y
G28 Z0                   ; home Z
G1 Z15.0 F{speed_travel} ; move extruder up
M117 Heating...                     ; progress indicator message on LCD
M109 S{material_print_temperature_layer_0}  ; wait for extruder to reach printing temp
M190 S{material_bed_temperature_layer_0}    ; wait for bed to reach printing temp

M117 Cleaning extruder...
G92 E0 ;Reset Extruder
G1 Z2.0 F3000 ;Move Z Axis up
G1 X10.1 Y20 Z0.28 F5000.0 ;Move to start position
G1 X10.1 Y200.0 Z0.28 F1500.0 E15 ;Draw the first line
G1 X10.4 Y200.0 Z0.28 F5000.0 ;Move to side a little
G1 X10.4 Y20 Z0.28 F1500.0 E30 ;Draw the second line
G92 E0 ;Reset Extruder
G1 Z2.0 F3000 ;Move Z Axis up

G92 E0                   ; set extruder position to 0
G1 F200 E0               ; prime the nozzle with filament
G92 E0                   ; re-set extruder position to 0
G1 F{speed_travel}       ; set travel speed
M203 X192 Y208 Z3        ; set limits on travel speed
M117 TAZ 5 Printing...   ; progress indicator message on LCD
